# React-Django app
Simple setup for a React-Django web app following tutorial on medium

## Setup
- Download/clone repo.
- Create and activate a virtual environment e.g. `pipenv shell`.
- Install Django with `pip install django`.
- Run Django app using `python manage.py runserver`.
- Install React dependencies with `npm install`. 
- Run React app with `npm start`.
- Build React app using `npm run build`.